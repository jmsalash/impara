import Vue from 'vue'
import Vuex from 'vuex'
import localForage from 'localforage'
import deck from '@/assets/Core_2000/deck.json'
import firebase from 'firebase'
import ImparaDataService from '@/services/ImparaDataService'
const levenshtein = require('js-levenshtein')
// const firebaseAdmin = require('firebase-admin')

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    openChat: false,
    userChat: { id: '', nickname: 'No one' },
    newMsgNotifications: {},
    newMsgListener: null,
    thisChatRef: [],
    thatChatRef: [],
    thatUserChatMessages: [],
    thisUserChatMessages: [],
    pos: [],
    chatMessages: [],
    nickName: '',
    userKeepSync: '',
    userStatus: {},
    userPresence: '',
    isLoggedIn: false,
    nWordsPct: { Dictionary: 0, Known: 0, Similar: 0, Archived: 0 },
    nWords: { Dictionary: 0, Known: 0, Similar: 0, Archived: 0 },
    threshold: 1,
    similarityNeedsUpdate: false,
    knownWords: {},
    archivedWords: {},
    similarWordIds: [],
    knownNotes: [],
    similarNotes: [],
    archivedNotes: [],
    archivedWordIds: [],
    mediaPath: '/media/',
    headers: [],
    reducedHeaders: [],
    jpHeaders: [],
    enHeaders: [],
    notes: [],
    dictSortBy: 'Optimized-Voc-Index',
    dictSortDesc: false,
    knownSortBy: 'Optimized-Voc-Index',
    knownSortDesc: false,
    similarSortBy: 'Optimized-Voc-Index',
    similarSortDesc: false,
    // mainHeaders: ['Optimized-Voc-Index', 'Frequency', 'Vocabulary-Kanji', 'Vocabulary-Furigana', 'Vocabulary-Kana', 'Vocabulary-English'],
    // mainHeaders: ['Optimized-Voc-Index', 'Frequency', 'Vocabulary-Kanji', 'Vocabulary-Kana', 'Vocabulary-English'],
    // mainHeaders: ['Optimized-Voc-Index', 'Frequency', 'Vocabulary-Kanji', 'Vocabulary-Kana', 'Vocabulary-English', 'Vocabulary-Pos'],
    mainHeaders: ['Optimized-Voc-Index', 'Frequency', 'Vocabulary-Kanji', 'Vocabulary-Kana', 'Vocabulary-English'],
    reducedHeadersList: ['Vocabulary-Kanji', 'Vocabulary-Kana', 'Vocabulary-English'],
    jpHeadersList: ['Vocabulary-Kanji', 'Vocabulary-Kana'],
    enHeadersList: ['Vocabulary-English'],
    mainHeadersNames: {
      'Vocabulary-Pos': 'Pos',
      'Optimized-Voc-Index': 'ID',
      Frequency: 'Frequency',
      'Vocabulary-Kanji': 'Kanji',
      'Vocabulary-Furigana': 'Furigana',
      'Vocabulary-Kana': 'Kana',
      'Vocabulary-English': 'English'
    },
    mainHeadersWidths: {
      'Vocabulary-Pos': '10%',
      'Vocabulary-Kanji': '10%',
      'Vocabulary-Kana': '20%',
      'Vocabulary-English': '30%'
    },
    extraHeaders: ['Vocabulary-Audio', 'Reading', 'Sentence-English', 'Sentence-Audio']
    //   extraHeaders: ['Vocabulary-Audio', 'Vocabulary-Pos', 'Expression', 'Reading', 'Sentence-Kana', 'Sentence-English', 'Sentence-Audio']
  },
  mutations: {
    getUserWords (state, userInfo) {
      const userID = userInfo.key
      const userName = userInfo.nickName
      // console.log(state.knownWords)
      ImparaDataService.userExists(userID).then(function (val) {
        // console.log(val.val())
        if (val.val()) {
          // console.log('getting user dict ', userID)
          ImparaDataService.getUserKnown(userID).then(function (wordList) {
            state.nWords.Similar = 0
            state.nWords.SimilarPct = 0
            state.showSimilar = false
            state.rivalUser = userName
            state.similarNotes = []
            localForage.removeItem('similarNotes')
            // console.log('Index dictionary ', wordList)
            let counter = 0
            deck.notes.forEach(function (n) {
              const obj = {}
              const extra = {}
              deck.note_models[0].flds.forEach((element, index) => {
                if (state.mainHeaders.includes(element.name)) obj[element.name] = n.fields[index]
                else extra[element.name] = n.fields[index].indexOf('.mp3') > -1 ? state.mediaPath + n.fields[index].replace('[sound:', '').replace(']', '') : n.fields[index]
              })
              obj.extra = extra
              obj.guid = n.guid
              obj.index = counter
              counter += 1
              if (obj['Optimized-Voc-Index'] in wordList) {
                // console.log(obj['Optimized-Voc-Index'], n.guid in state.knownWords, n)
                if (!(n.guid in state.knownWords)) {
                  state.similarNotes.push(obj)
                }
              }
            })
            // Counters
            state.nWordsPct.Similar = Math.floor(100 * state.similarNotes.length / state.knownNotes.length)
            state.nWords.Similar = '' + String(state.similarNotes.length) + '/' + state.knownNotes.length
          })
        }
      })
    },
    updateNickname (state, nickName) {
      firebase.auth().currentUser
        .updateProfile({
          displayName: nickName
        }).then(function () {
          // Update successful.
          console.log('user updated')
          ImparaDataService.updateStats(firebase.auth().currentUser, deck.notes.length, state.knownNotes.length, state.archivedNotes.length, state.archivedNotes.length)
          state.nickName = firebase.auth().currentUser.displayName.substring(1)
          state.shareStats = parseInt(firebase.auth().currentUser.displayName.substring(0, 1))
        }, function (error) {
          console.log('profile error', error)
        })
    },
    updateStatSharing (state, value) {
      firebase.auth().currentUser
        .updateProfile({
          phoneNumber: value
        }).then(function () {
          // Update successful.
          ImparaDataService.updateStats(firebase.auth().currentUser, deck.notes.length, state.knownNotes.length, state.archivedNotes.length)
          state.user = firebase.auth().currentUser
          console.log('user updated', state.user)
        }, function (error) {
          console.log('profile error', error)
        })
    },
    loggedOut (state) {
      state.isLoggedIn = false
      state.userKeepSync.set(false).then(() => console.log('Online presence set'))
      state.newMsgNotifications = {}
      state.newMsgListener.off()
    },
    loggedIn (state) {
      state.isLoggedIn = true
      state.nickName = firebase.auth().currentUser.displayName.substring(1)
      state.shareStats = parseInt(firebase.auth().currentUser.displayName.substring(0, 1))
      state.user = firebase.auth().currentUser
    },
    deleteSimilar (state) {
      state.similarNotes = []
      state.showSimilar = true
      localForage.removeItem('similarNotes')
      state.nWords.Similar = 0
      state.nWords.SimilarPct = 0
    },
    getSimilarNotes (state, threshold = 0) {
      state.showSimilar = true
      state.rivalUser = ''
      state.similarNotes = []
      state.similarWordIds = []
      let l = 100
      let length = 0
      state.notes.forEach((note, index) => {
        if (!state.similarWordIds.includes(note.guid)) {
          state.knownNotes.forEach(kNote => {
            // console.log(note['Vocabulary-Kana'], kNote['Vocabulary-Kana'])
            l = levenshtein(note['Vocabulary-Kana'], kNote['Vocabulary-Kana'])
            length = Math.min(note['Vocabulary-Kana'].length, kNote['Vocabulary-Kana'].length)
            if ((length - l) / length >= threshold) {
              // console.log(l, note['Vocabulary-Kana'], kNote['Vocabulary-Kana'])
              if (note.guid !== kNote.guid && !state.similarWordIds.includes(note.guid)) {
                note.extra.similarFurigana = note.extra.similarFurigana ? note.extra.similarFurigana : ' | ' + kNote.extra['Vocabulary-Furigana'] + ' | '
                note.extra.similarEnglish = note.extra.similarEnglish ? note.extra.similarEnglish : ' | ' + kNote['Vocabulary-English'] + ' | '
                note.dictIndex = index
                // console.log(note)
                state.similarNotes.push(note)
                state.similarWordIds.push(note.guid)
              }
            }
          })
        }
      })
      state.nWordsPct.Similar = Math.floor(100 * state.similarNotes.length / deck.notes.length)
      state.nWords.Similar = '' + String(state.similarNotes.length) + '/' + deck.notes.length
      localForage.setItem('similarNotes', state.similarNotes)
    },
    updatedictSortBy (state, c) {
      if (c) {
        state.dictSortBy = c
        localForage.setItem('dictSortBy', c)
      }
    },
    updatedictSortDesc (state, c) {
      if (c) {
        state.dictSortDesc = c
        localForage.setItem('dictSortDesc', c)
      }
    },
    updateknownSortBy (state, c) {
      if (c) {
        state.knownSortBy = c
        localForage.setItem('dictKnownSortBy', c)
      }
    },
    updateknownSortDesc (state, c) {
      if (c) {
        state.knownSortDesc = c
        localForage.setItem('dictKnownSortDesc', c)
      }
    },
    updatesimilarSortBy (state, c) {
      if (c) {
        state.similarSortBy = c
        localForage.setItem('dictSimilarSortBy', c)
      }
    },
    updatesimilarSortDesc (state, c) {
      if (c) {
        state.similarSortDesc = c
        localForage.setItem('dictSimilarSortDesc', c)
      }
    },
    addSimilar (state, w) {
      // console.log(w.word.guid, w.word, state.similarNotes.indexOf(w), w.index)
      state.knownWords[w.word.guid] = w.word
      state.knownNotes.push(w.word)
      localForage.setItem('knownWords', state.knownWords)
      state.similarNotes.splice(w.index, 1)
      // console.log(state.notes.indexOf(w.word), w.word.dictIndex)
      state.notes.splice(w.word.dictIndex, 1)
      state.nWordsPct.Similar = Math.floor(100 * state.similarNotes.length / deck.notes.length)
      state.nWords.Similar = '' + String(state.similarNotes.length) + '/' + deck.notes.length
      state.nWordsPct.Dictionary = Math.floor(100 * state.notes.length / deck.notes.length)
      state.nWords.Dictionary = '' + String(state.notes.length) + '/' + deck.notes.length
    },
    addWord (state, w) {
      ImparaDataService.update(state.userID, w.word['Optimized-Voc-Index'], 'bowID', 1)
      // console.log(w.word.guid, w.word)
      state.knownWords[w.word.guid] = w.word
      state.knownNotes.push(w.word)
      localForage.setItem('knownWords', state.knownWords)
      // localForage.getItem('knownWords').then(function (item) {
      //   console.log(item)
      // })
      state.notes.splice(w.index, 1)
      state.nWordsPct.Dictionary = Math.floor(100 * state.notes.length / deck.notes.length)
      state.nWords.Dictionary = '' + String(state.notes.length) + '/' + deck.notes.length
      state.nWordsPct.Known = Math.floor(100 * state.knownNotes.length / deck.notes.length)
      state.nWords.Known = '' + String(state.knownNotes.length) + '/' + deck.notes.length
      if (firebase.auth().currentUser) ImparaDataService.updateStats(firebase.auth().currentUser, deck.notes.length, state.knownNotes.length, state.archivedNotes.length)
    },
    archiveWord (state, w) {
      ImparaDataService.update(state.userID, w.word['Optimized-Voc-Index'], 'bowID', 2)
      // console.log(w.word.guid, w.word)
      state.archivedWords[w.word.guid] = w.word
      state.archivedNotes.push(w.word)
      localForage.setItem('archivedWords', state.knownWords)
      // localForage.getItem('knownWords').then(function (item) {
      //   console.log(item)
      // })
      state.notes.splice(w.index, 1)
      state.nWordsPct.Dictionary = Math.floor(100 * state.notes.length / deck.notes.length)
      state.nWords.Dictionary = '' + String(state.notes.length) + '/' + deck.notes.length
      state.nWordsPct.Archived = Math.floor(100 * state.archivedNotes.length / deck.notes.length)
      state.nWords.Archived = '' + String(state.archivedNotes.length) + '/' + deck.notes.length
      if (firebase.auth().currentUser) ImparaDataService.updateStats(firebase.auth().currentUser, deck.notes.length, state.knownNotes.length, state.archivedNotes.length)
    },
    removeWord (state, w) {
      ImparaDataService.update(state.userID, w.word['Optimized-Voc-Index'], 'bowID', 0)
      delete (state.knownWords[w.word.guid])
      state.knownNotes.splice(w.index, 1)
      state.notes.push(w.word)
      localForage.setItem('knownWords', state.knownWords)
      state.nWordsPct.Dictionary = Math.floor(100 * state.notes.length / deck.notes.length)
      state.nWords.Dictionary = '' + String(state.notes.length) + '/' + deck.notes.length
      state.nWordsPct.Known = Math.floor(100 * state.knownNotes.length / deck.notes.length)
      state.nWords.Known = '' + String(state.knownNotes.length) + '/' + deck.notes.length
      if (firebase.auth().currentUser) ImparaDataService.updateStats(firebase.auth().currentUser, deck.notes.length, state.knownNotes.length, state.archivedNotes.length)
    },
    async removeArchived (state, w) {
      // Remove from archived
      ImparaDataService.update(state.userID, w.word['Optimized-Voc-Index'], 'bowID', 0)
      delete (state.archivedWords[w.word.guid])
      state.archivedNotes.splice(w.index, 1)
      state.notes.push(w.word)
      localForage.setItem('archivedWords', state.archivedWords)
      state.nWordsPct.Dictionary = Math.floor(100 * state.notes.length / deck.notes.length)
      state.nWords.Dictionary = '' + String(state.notes.length) + '/' + deck.notes.length
      state.nWordsPct.Archived = Math.floor(100 * state.archivedNotes.length / deck.notes.length)
      state.nWords.Archived = '' + String(state.archivedNotes.length) + '/' + deck.notes.length
      if (firebase.auth().currentUser) await ImparaDataService.updateStats(firebase.auth().currentUser, deck.notes.length, state.knownNotes.length, state.archivedNotes.length)

      // Add to known
      ImparaDataService.update(state.userID, w.word['Optimized-Voc-Index'], 'bowID', 1)
      state.knownWords[w.word.guid] = w.word
      state.knownNotes.push(w.word)
      localForage.setItem('knownWords', state.knownWords)
      state.notes.splice(w.index, 1)
      state.nWordsPct.Dictionary = Math.floor(100 * state.notes.length / deck.notes.length)
      state.nWords.Dictionary = '' + String(state.notes.length) + '/' + deck.notes.length
      state.nWordsPct.Known = Math.floor(100 * state.knownNotes.length / deck.notes.length)
      state.nWords.Known = '' + String(state.knownNotes.length) + '/' + deck.notes.length
      if (firebase.auth().currentUser) ImparaDataService.updateStats(firebase.auth().currentUser, deck.notes.length, state.knownNotes.length, state.archivedNotes.length)
    },
    restoreStoredSimilarNotes (state) {
      localForage.getItem('similarNotes', function (err, listOfWords) {
        if (err) console.log(err)
        else {
          // console.log(listOfWords)
          if (listOfWords) state.similarNotes = listOfWords
          state.showSimilar = true
        }
      })
    },
    userExists (state) {
      const prom = ImparaDataService.userExists(firebase.auth().currentUser.uid)
      prom.then(function (val) {
        state.userExists = val
      })
      // console.log(state.userExists)
    },
    initDictionaryFromDB (state, wordList) {
      state.notes = []
      state.knownNotes = []
      state.knownWords = []
      state.pos = []
      deck.notes.forEach(function (n) {
        const obj = {}
        const extra = {}
        deck.note_models[0].flds.forEach((element, index) => {
          if (state.mainHeaders.includes(element.name)) obj[element.name] = n.fields[index]
          else extra[element.name] = n.fields[index].indexOf('.mp3') > -1 ? state.mediaPath + n.fields[index].replace('[sound:', '').replace(']', '') : n.fields[index]
        })
        obj.extra = extra
        obj.guid = n.guid
        // if(state.pos.contains(''))
        if (state.pos.indexOf(obj['Vocabulary-Pos']) === -1) state.pos.push(obj['Vocabulary-Pos'])
        if (wordList[obj['Optimized-Voc-Index']].bowID === 1) {
          state.knownNotes.push(obj)
          state.knownWords[n.guid] = {}
        } else if (wordList[obj['Optimized-Voc-Index']].bowID === 0) {
          state.notes.push(obj)
        } else {
          state.archivedNotes.push(obj)
          state.archivedWords[n.guid] = {}
        }
      })
      // Counters
      state.nWordsPct.Dictionary = Math.floor(100 * state.notes.length / deck.notes.length)
      state.nWords.Dictionary = '' + String(state.notes.length) + '/' + deck.notes.length
      state.nWordsPct.Known = Math.floor(100 * state.knownNotes.length / deck.notes.length)
      state.nWords.Known = '' + String(state.knownNotes.length) + '/' + deck.notes.length
      state.nWordsPct.Archived = Math.floor(100 * state.archivedNotes.length / deck.notes.length)
      state.nWords.Archived = '' + String(state.archivedNotes.length) + '/' + deck.notes.length
    },
    initDictionary (state) {
      console.log('init list')
      console.log('get stored words')

      if (state.notes.length > 0) return
      localForage.getItem('knownWords', function (err, listOfWords) {
        if (err) console.log(err)
        else {
          // console.log(listOfWords)
          if (listOfWords) state.knownWords = listOfWords
          // state.headers = [{ text: '', value: 'data-table-expand', width: '5%' }]
          state.headers = []
          deck.note_models[0].flds.map(f => {
            if (state.mainHeaders.includes(f.name)) {
              const myAlign = f.name === 'Optimized-Voc-Index' || f.name === 'Frequency' ? ' d-none d-xs-table-cell' : ''
              const mShow = !(f.name === 'Optimized-Voc-Index' || f.name === 'Frequency')
              // console.log(myAlign)
              const header = {
                text: state.mainHeadersNames[f.name],
                sortable: true,
                value: f.name,
                align: myAlign,
                show: mShow,
                width: state.mainHeadersWidths[f.name]
              }
              state.headers.push(header)
              if (state.enHeadersList.includes(f.name)) {
                state.enHeaders.push(header)
              }
              if (state.jpHeadersList.includes(f.name)) {
                state.jpHeaders.push(header)
              }
              if (state.reducedHeadersList.includes(f.name)) {
                state.reducedHeaders.push(header)
              }
            }
          })
          // state.headers.push({ text: '', value: 'play', sortable: false })
          state.headers.push({ text: 'Actions', align: 'center', value: 'actions', sortable: false, width: '15%' })
          state.enHeaders.push({ text: 'Actions', align: 'center', value: 'actions', sortable: false, width: '15%' })
          state.jpHeaders.push({ text: 'Actions', align: 'center', value: 'actions', sortable: false, width: '15%' })
          state.reducedHeaders.push({ text: 'Actions', align: 'center', value: 'actions', sortable: false, width: '15%' })
          // console.log('kc`&lV:nE)' in state.knownWords)
          deck.notes.forEach(function (n) {
            const obj = {}
            const extra = {}
            deck.note_models[0].flds.forEach((element, index) => {
              if (state.mainHeaders.includes(element.name)) obj[element.name] = n.fields[index]
              else extra[element.name] = n.fields[index].indexOf('.mp3') > -1 ? state.mediaPath + n.fields[index].replace('[sound:', '').replace(']', '') : n.fields[index]
              if (element.name === 'Vocabulary-Pos') {
                if (state.pos.indexOf(obj['Vocabulary-Pos']) === -1) state.pos.push(obj['Vocabulary-Pos'])
              }
            })
            obj.extra = extra
            obj.guid = n.guid
            if (n.guid in state.knownWords) {
              state.knownNotes.push(obj)
            } else {
              state.notes.push(obj)
            }
          })
          // Counters
          state.nWordsPct.Dictionary = Math.floor(100 * state.notes.length / deck.notes.length)
          state.nWords.Dictionary = '' + String(state.notes.length) + '/' + deck.notes.length
          state.nWordsPct.Known = Math.floor(100 * state.knownNotes.length / deck.notes.length)
          state.nWords.Known = '' + String(state.knownNotes.length) + '/' + deck.notes.length
          localForage.getItem('dictSortBy').then(function (c) {
            state.dictSortBy = c
          })
          localForage.getItem('dictSortDesc').then(function (c) {
            state.dictSortDesc = c
          })
          localForage.getItem('dictKnownSortBy').then(function (c) {
            state.knownSortBy = c
          })
          localForage.getItem('dictKnownSortDesc').then(function (c) {
            state.knownSortDesc = c
          })
          localForage.getItem('dictSimilarSortBy').then(function (c) {
            state.similarSortBy = c
          })
          localForage.getItem('dictSimilarSortDesc').then(function (c) {
            state.similarSortDesc = c
          })
          localForage.getItem('similarityNeedsUpdate').then(function (b) {
            state.similarityNeedsUpdate = b
          })
        }
      })
    },
    setChatUsr (state, toUsr) {
      state.openChat = true
      state.userChat = toUsr
    },
    closeChat (state) {
      state.openChat = false
    },
    sendChatMsg (state, msg) {
      // console.log(msg)
      ImparaDataService
        .sendChatMsg(msg)
    },
    notificationListener (state) {
      // Listen for new messangers from other users
      state.newMsgListener = ImparaDataService.listenChat(state.userID)
      state.newMsgListener.on('value', (snapshot) => {
        if (snapshot.exists()) {
          state.newMsgNotifications = snapshot.val()
        } else {
          state.newMsgNotifications = {}
        }
      })
    },
    getChatMsgs (state, otherUsrId) {
      // console.log(otherUsrId, state.userChat)
      ImparaDataService
        .getChatMsg(state.userID, otherUsrId)
        .then(function (me) {
          if (me) {
            // console.log(me)
            state.thisUserChatMessages = me
            // console.log('sent msgs ', state.thisUserChatMessages)
          } else {
            state.thisUserChatMessages = []
          }
          // Get messages
          ImparaDataService
            .getChatMsg(otherUsrId, state.userID)
            .then(function (you) {
              state.thatUserChatMessages = you
              // console.log('received msgs', state.thatUserChatMessages)
              const sortTemp = {
                ...state.thisUserChatMessages,
                ...state.thatUserChatMessages
              }
              state.chatMessages = Object.fromEntries(Object.entries(sortTemp).sort())
              state.thatChatRef = ImparaDataService.getChatRef(state.userID, otherUsrId) // messages from other users
              state.thisChatRef = ImparaDataService.getChatRef(otherUsrId, state.userID) // my messages to the user
              state.thisChatRef.on('value', (snapshot) => {
                if (snapshot.exists()) {
                  const temp = snapshot.val()
                  state.thatUserChatMessages = temp
                  const sortTemp = {
                    ...state.thisUserChatMessages,
                    ...state.thatUserChatMessages
                  }
                  state.chatMessages = Object.fromEntries(Object.entries(sortTemp).sort())
                  // console.log('to', snapshot.val())
                  // console.log('from', snapshot.val())
                }
              })

              state.thatChatRef.on('value', (snapshot) => {
                if (state.openChat && state.userChat.id === otherUsrId) {
                  console.log('Clear notification for user ')
                  ImparaDataService.clearNotification(state.userID, otherUsrId)
                }
                if (snapshot.exists()) {
                  const temp = snapshot.val()
                  state.thisUserChatMessages = temp
                  const sortTemp = {
                    ...state.thisUserChatMessages,
                    ...state.thatUserChatMessages
                  }
                  state.chatMessages = Object.fromEntries(Object.entries(sortTemp).sort())
                  // console.log('to', snapshot.val())
                }
              })
            })
        }
        )
    }
  },
  actions: {
    openChat (context, toUsr) {
      context.commit('setChatUsr', toUsr)
      ImparaDataService.clearNotification(context.state.userID, toUsr.id)

      // let messageForm = {
      //   content: '',
      //   toUsr: toUsr.id,
      //   fromUsr: context.state.userID,
      //   timestamp: '0'
      // }
      // context.commit('sendChatMsg', messageForm) // Init chat
      // messageForm = {
      //   content: '',
      //   fromUsr: toUsr.id,
      //   toUsr: context.state.userID,
      //   timestamp: '0'
      // }
      // context.commit('sendChatMsg', messageForm) // Init chat
      context.commit('getChatMsgs', toUsr.id)
    },
    initStore (context) {
      // context.commit('initKnownWords')
      context.commit('initDictionary')
      context.commit('restoreStoredSimilarNotes')
    },
    getSimilarNotes (context, threshold = 0) {
      context.commit('getSimilarNotes', threshold)
    },
    loggedIn (context) {
      console.log('run logged in')
      context.commit('loggedIn')
      context.state.userID = firebase.auth().currentUser.uid
      context.commit('notificationListener')
      context.state.userKeepSync = ImparaDataService.userOnline(context.state.userID)
      // User status auto update
      context.state.userPresence = ImparaDataService.getUserStatus()
      context.state.userPresence.on('value', (snapshot) => {
        if (snapshot.exists()) {
          context.state.userStatus = snapshot.val()
        }
      })
      ImparaDataService.userExists(context.state.userID).then(function (val) {
        context.state.userExists = val
        // console.log(val.val())
        if (val.val()) {
          ImparaDataService.getUserDictionary(context.state.userID).then(function (d) {
            // console.log('Index dictionary ', d)
            context.commit('initDictionaryFromDB', d)
          })
        } else {
          console.log('Init user db')
          ImparaDataService.createDictionary(context.state.notes, 0, firebase.auth().currentUser.uid)
          ImparaDataService.createDictionary(context.state.knownNotes, 1, firebase.auth().currentUser.uid)
        }
      })
    }
  },
  modules: {
  }
})
