import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import { auth } from '@/helpers/firebase'
// import firebase from 'firebase'
// import firebaseApp from 'firebase/app'
// import 'firebase/database'
// // import 'firebase/database'

// // Your web app's Firebase configuration
// var firebaseConfig = {
//   apiKey: 'AIzaSyCOlY2NgGfcY9GbjwILl1ur9uRIXvxbOG0',
//   authDomain: 'imparajp.firebaseapp.com',
//   databaseURL: 'https://imparajp-default-rtdb.europe-west1.firebasedatabase.app',
//   projectId: 'imparajp',
//   storageBucket: 'imparajp.appspot.com',
//   messagingSenderId: '771075612455',
//   appId: '1:771075612455:web:e92df62041a698dd0d1302',
//   measurementId: 'G-4HRZHGTSRL'
// }
// // Initialize Firebase
// firebaseApp.initializeApp(firebaseConfig)

Vue.config.productionTip = false

let app
auth.onAuthStateChanged(async user => {
  if (!app) {
    await auth.currentUser
    // console.log('main ', user, user2)
    app = new Vue({
      router,
      store,
      vuetify,
      created () {
        this.$store.dispatch('initStore')
        if (!user) {
          this.$router.push('/')
        } else {
          this.$store.dispatch('loggedIn')
        }
      },
      render: h => h(App)
    }).$mount('#app')
  }
})

// new Vue({
//   router,
//   store,
//   vuetify,
//   created () {
//     this.$store.dispatch('initStore')
//   },
//   render: h => h(App)
// }).$mount('#app')

// TODO
// Hide English translation
// Reset sorting criteria
// Reset all words
// offline firebase
