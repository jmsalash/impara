import firebase from 'firebase/app'
import 'firebase/database'

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: 'AIzaSyCOlY2NgGfcY9GbjwILl1ur9uRIXvxbOG0',
  authDomain: 'imparajp.firebaseapp.com',
  databaseURL: 'https://imparajp-default-rtdb.europe-west1.firebasedatabase.app',
  projectId: 'imparajp',
  storageBucket: 'imparajp.appspot.com',
  messagingSenderId: '771075612455',
  appId: '1:771075612455:web:e92df62041a698dd0d1302',
  measurementId: 'G-4HRZHGTSRL'
}
// Initialize Firebase
firebase.initializeApp(firebaseConfig)

const AppDB = firebase.database()
export { AppDB } // Make it available to other modules
