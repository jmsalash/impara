// import { AppDB } from '@/db-init.js'
import { AppDB } from '@/helpers/firebase'
// import firebase from 'firebase'
// const AppDB = firebase.database()
const db = AppDB.ref('/imparajp')

class ImparaDataService {
  sendChatMsg (msg) {
    AppDB.ref(`imparajp/chats/${msg.toUsr}/${msg.fromUsr}`).push(msg)// .child(msg.timestamp).set(msg) //
    AppDB.ref(`imparajp/chats/notifications/${msg.toUsr}/msgs/${msg.fromUsr}`).set(true)
  }

  getChatMsg (fromUsr, toUsr) {
    return AppDB.ref(`imparajp/chats/${fromUsr}/${toUsr}`)
      // .orderByChild('toUsr').equalTo(toUsr)
      .limitToLast(10)
      .get()
      .then(function (snapshot) {
        return snapshot.val()
      })
  }

  clearNotification (fromUsr, toUsr) {
    AppDB.ref(`imparajp/chats/notifications/${fromUsr}/msgs/${toUsr}`).remove()
  }

  listenChat (toUsr) {
    return AppDB.ref(`imparajp/chats/notifications/${toUsr}/msgs/`)
  }

  sendChatNotification (fromUsr, toUsr) {
    AppDB.ref(`imparajp/chats/${toUsr}/${fromUsr}`).set(true)// .child(msg.timestamp).set(msg) //
  }

  getChatRef (fromUsr, toUsr) {
    return AppDB.ref(`imparajp/chats/${fromUsr}/${toUsr}`).orderByChild('timestamp')// .equalTo(toUsr)
  }

  userOnline (userID) {
    const userPresence = AppDB.ref(`imparajp/online/${userID}`)
    // AppDB.ref(`imparajp/chats/notifications/${userID}/msgs/flag`).set(true)
    userPresence.set(true).then(() => console.log('Online presence set'))
    userPresence
      .onDisconnect()
      .remove()
      .then(() => console.log('On disconnect function configured.'))
    return userPresence
  }

  getUserStatus () {
    return AppDB.ref('imparajp/online/')
  }

  getAll () {
    return db
  }

  getUserDictionary (userID) {
    return AppDB.ref(`imparajp/${userID}`).get().then((snapshot) => {
      // console.log(`imparajp/${userID}/wordID`, snapshot.val())
      if (snapshot.exists()) {
        // console.log('got user data')
        return snapshot.val()
      }
    })
  }

  getUsersStats () {
    return AppDB.ref('imparajp/stats').orderByChild('shareStats').equalTo(1)
  }

  updateStats (user, total, known, archived) {
    const newPair = {
      total: total,
      known: known,
      nickName: user.displayName.substring(1),
      shareStats: parseInt(user.displayName.substring(0, 1)),
      archived: archived
    }
    return AppDB.ref('imparajp/stats/').child(user.uid).update(newPair)
  }

  userExists (userID) {
    // every user must have an email
    return AppDB.ref(`imparajp/${userID}`).once('value', snapshot => {
      // console.log(`imparajp/${userID}`, snapshot.val())
      if (snapshot.exists()) {
        console.log('exists!')
        return true
      } else {
        console.log('no user data')
        return false
      }
    })
  }

  getUserKnown (userID) {
    return AppDB.ref(`imparajp/${userID}`).orderByChild('bowID').equalTo(1).get().then((snapshot) => {
      // console.log(`imparajp/${userID}/wordID`, snapshot.val())
      if (snapshot.exists()) {
        console.log('got user data')
        return snapshot.val()
      }
    })
  }

  createDictionary (dictionary, bowID, user) {
    // const dict = []
    // console.log(dictionary, bowID, user)
    dictionary.forEach(element => {
      // dict.push({
      //   key: element.guid + '_' + user,
      //   userID: user,
      //   wordID: element.guid,
      //   bowID: bowID // 0=dictionary, 1=known word})
      // })
      AppDB.ref('/imparajp/').child(user).child(element['Optimized-Voc-Index']).set({
        // wordID: element['Optimized-Voc-Index'],
        bowID: bowID // 0=dictionary, 1=known word})
        // key: element.guid + '_' + user,
        // userID: user,
        // wordID: element.guid,
      })
    })
    // return AppDB.ref('/imparajp/').child('words').set(dict)
  }

  update (userID, wordID, key, value) {
    const newPair = {}
    newPair[key] = value
    return AppDB.ref(`imparajp/${userID}`).child(wordID).update(newPair)
  }

  delete (key) {
    return db.child(key).remove()
  }

  deleteAll () {
    return db.remove()
  }
}

export default new ImparaDataService()
