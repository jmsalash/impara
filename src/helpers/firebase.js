import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

var firebaseConfig = {
  apiKey: 'AIzaSyCOlY2NgGfcY9GbjwILl1ur9uRIXvxbOG0',
  authDomain: 'imparajp.firebaseapp.com',
  databaseURL: 'https://imparajp-default-rtdb.europe-west1.firebasedatabase.app',
  projectId: 'imparajp',
  storageBucket: 'imparajp.appspot.com',
  messagingSenderId: '771075612455',
  appId: '1:771075612455:web:e92df62041a698dd0d1302',
  measurementId: 'G-4HRZHGTSRL'
}

firebase.initializeApp(firebaseConfig)

// utils
const AppDB = firebase.database()
const auth = firebase.auth()
auth.setPersistence(firebase.auth.Auth.Persistence.LOCAL)

// export utils/refs
export {
  AppDB,
  auth
}
